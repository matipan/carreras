# Primer año
## 1er cuatrimestre
* [x] Matematica A
* [x] Programacion 1
* [ ] Quimica
  
## 2do cuatrimestre
* [x] Matematica B
* [x] Programacion 2
* [ ] Sistemas de representacion

# Segundo año
## 1er cuatrimestre
* [ ] **Fisica 1** AHORA
* [x] Programacion 3
* [x] Taller de lenguaje 1
* [x] Concepto de arquitectura de computadores
  
## 2do cuatrimestre
* [x] Matematica C
* [ ] Fisica 2
* [x] Conceptos de sistemas operativos(**FALTA FINAL**)
* [x] Taller de lenguajes 2
* [x] Probabilidades
* [x] Ingles

# Tercer año
## 1er cuatrimestre
* [ ] Matematica d
* [ ] Electrotecnia y electronica
* [ ] **Conceptos de bases de datos** AHORA
* [x] Introduccion al diseño logico
* [x] Estadistica(**FALTA FINAL**)  

## 2do cuatrimestre
* [ ] Introduccion al procesamiento de señales
* [x] Taller de arquitectura
* [ ] Ingenieria de Software
* [x] Redes de datos 1(**FALTA FINAL**)

# Cuarto año
## 1er cuatrimestre
* [ ] **Concurrencia y paralelismo** AHORA
* [ ] Instrumentacion y control
* [ ] Bases de datos
* [ ] **Circuitos digitales y microcontroladores** AHORA
* [x] Economia y emprendedorismo

## 2do cuatrimestre
* [ ] Redes de datos 2
* [ ] Sistemas de tiempo real
* [ ] Taller de proyecto 1
* [ ] Optativa 1

# Quinto año
## 1er cuatrimestre
* [ ] Sistemas distribuidos y paralelos
* [ ] **Aspectos legales de ingenieria informatica** AHORA
* [ ] Optativa 2
* [ ] Optativa 3

## 2do cuatrimestre
* [ ] Taller de proyecto 2
* [ ] Seminario de redaccion de textos profesionales
* [ ] Optativa 4
* [ ] **PPS** AHORA

### TOTAL: 17/42 materias  -  40%
