# Primer año
## 1er cuatrimestre
* [x] Conceptos de Algoritmos, Datos y Programas
* [x] Organizacion de Computadoras
* [x] Matematica 1

## 2do cuatrimestre
* [x] Taller de programacion
* [x] Arquitectura de Computadoras
* [x] Matematica 2

# Segundo año
## 1er cuatrimestre
* [ ] **Fundamentos de Organizacion de Datos** AHORA
* [x] Algoritmos y Estructuras de datos
* [x] Seminario de Lenguajes

## 2do cuatrimestre
* [ ] Diseño de bases de datos
* [ ] Ingenieria de software 1
* [x] Orientacion a Objetos 1
* [x] Introduccion a los sistemas operativos(**FALTA FINAL**)
* [x] Taller de ingles

# Tercer año
## 1er cuatrimestre
* [x] Matematica 3
* [ ] Ingenieria de Software 2
* [ ] Orientacion a objetos 2

## 2do cuatrimestre
* [ ] Conceptos y Paradigmas de Lenguajes de Programacion
* [ ] Taller de tecnologias de produccion de software
* [x] Redes y comunicaciones(**FALTA FINAL**)
* [ ] Proyecto de software
* [ ] **Programacion concurrente** AHORA
* [ ] Bases de datos 1
* [ ] Sistemas y organizaciones

### TOTAL: 13/24 materias  -  54%
