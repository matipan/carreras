# Primer año
## 1er cuatrimestre
* [x] Conceptos de Algoritmos, Datos y Programas
* [x] Organizacion de Computadoras
* [x] Matematica 1

## 2do cuatrimestre
* [x] Taller de programacion
* [x] Arquitectura de Computadoras
* [x] Matematica 2

# Segundo año
## 1er cuatrimestre
* [ ] **Fundamentos de Organizacion de Datos** AHORA
* [x] Algoritmos y Estructuras de datos
* [x] Seminario de Lenguajes

## 2do cuatrimestre
* [ ] Diseño de bases de datos
* [ ] Ingenieria de software 1
* [x] Orientacion a Objetos 1
* [x] Introduccion a los sistemas operativos(**FALTA FINAL**)
* [x] Taller de ingles

# Tercer año
## 1er cuatrimestre
* [x] Matematica 3
* [ ] Ingenieria de Software 2
* [ ] Orientacion a objetos 2
* [ ] Conceptos y Paradigmas de Lenguajes de Programacion

## 2do cuatrimestre
* [x] Redes y comunicaciones(**FALTA FINAL**)
* [ ] Proyecto de software
* [ ] **Programacion concurrente** AHORA
* [ ] Bases de datos 1

# Cuarto año
## 1er cuatrimestre
* [ ] Fundamentos de la teoria de la Computacion
* [ ] Sistemas operativos
* [ ] Bases de Datos 2
* [ ] Ingenieria de Software 3

## 2do cuatrimestre
* [ ] Sistemas y organizaciones
* [x] Matematica 4
* [ ] Desarrollo de software en Sistemas Distribuidos
* [ ] Optativa 1

# Quinto año
## 1er cuatrimestre
* [ ] **Aspectos sociales y profesionales de Informatica** AHORA
* [ ] Optativa 2
* [ ] Optativa 3

## Todo el año
* [ ] Tesina de Licenciatura en Sistemas

### TOTAL: 14/34 materias  -  42%
