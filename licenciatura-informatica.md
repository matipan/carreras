# Primer año
## 1er cuatrimestre
* [x] Conceptos de Algoritmos, Datos y Programas
* [x] Organizacion de Computadoras
* [x] Matematica 1

## 2do cuatrimestre
* [x] Taller de programacion
* [x] Arquitectura de Computadoras
* [x] Matematica 2

# Segundo año
## 1er cuatrimestre
* [ ] **Fundamentos de Organizacion de Datos** AHORA
* [x] Algoritmos y Estructuras de datos
* [x] Seminario de Lenguajes

## 2do cuatrimestre
* [ ] Diseño de bases de datos
* [ ] Ingenieria de software 1
* [x] Orientacion a Objetos 1
* [x] Introduccion a los sistemas operativos(**FALTA FINAL**)
* [x] Taller de ingles

# Tercer año
## 1er cuatrimestre
* [x] Matematica 3
* [ ] Ingenieria de Software 2
* [ ] Orientacion a objetos 2
* [ ] Conceptos y Paradigmas de Lenguajes de Programacion

## 2do cuatrimestre
* [x] Redes y comunicaciones(**FALTA FINAL**)
* [ ] Proyecto de software
* [ ] **Programacion concurrente** AHORA
* [ ] Computabilidad y complejidad

# Cuarto año
## 1er cuatrimestre
* [ ] Teoria de la computacion y verificacion de programas
* [ ] Sistemas operativos
* [ ] Sistemas paralelos
* [ ] Optativa 1 LI

## 2do cuatrimestre
* [ ] Logica e inteligencia artificial
* [x] Matematica 4
* [ ] Laboratorio de Software
* [ ] Programacion distribuida y tiempo real

# Quinto año
## 1er cuatrimestre
* [ ] Diseño de experiencia de usuario
* [ ] **Aspectos sociales y profesionales de Informatica** AHORA
* [ ] Optativa 2 LI

## Todo el año
* [ ] Tesina de Licenciatura en Informatica

### TOTAL: 14/34 materias  -  42%
